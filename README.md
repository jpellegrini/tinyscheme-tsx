This is a fork of TinyScheme Extensions (TSX) 1.1, originally written
by Manuel Heras-Gilsanz (manuel@heras-gilsanz.com).

According to the original README,

> TinyScheme Extensions is a set of dynamic libraries incorporating
> additional funcionality to TinyScheme, a lightweight
> implementation of the Scheme programming language. TinyScheme
> (http://tinyscheme.sourceforge.net) is maintained by D. Souflis
> (dsouflis@acm.org), and is based on MiniSCHEME version 0.85k4.
>
> Scheme is a very nice and powerful programming language, but the
> basic language is very minimalistic in terms of library functions;
> only basic file input / output functionality is specified.
> Different implementations of the language (MIT Scheme, GUILE,
> Bigloo...) provide their own extension libraries. TSX attempts to
> provide commonly needed functions at a small cost in terms of
> additional program footprint. The library is modularized, so that
> it is possible (and easy!) to select desired functionality via
> `#defines` in `tsx.h`.

Currently, Tinyscheme with TSX is a nice Scheme implementation for embedded
devices.

# Installation

Copy `tsx-xx.tar.gz` to the Tinyscheme source directory, where scheme.h is,
and run `make`. A file called `tsx.so` will be created, which
can be loaded as a TinyScheme extension:

```
(load-extension "tsx-1.0/tsx")
```

If you want to pick specific funcionality in order to obtain a
smaller binary, you can look into `tsx.h` and comment out
the `#define` lines that include modules you will not need.

Make sure you have enabled dynamic modules in your tinyscheme
runtime (define `USE_DL`  near the top in `scheme.h`).


# Sample applications

Three sample applications by Manuel Heras-Gilsanz are distributed with TSX 1.1,
in the `examples` subdirectory.

* `smtp.scm`
    Sends an email to the user getting the username from
    the USER shell variable, connecting to the SMTP port
    on the local machine.

* `listhome.scm`
    Provides a list of all the files on the user's home
    directory (obtained with the HOME environment variable).

* `srepl.scm`
    A socket-based REPL which listens
    for connections on the port 9000 and executes the commands
    received. To test it, start the server with `tinyscheme srepl.scm`
    and, in a different terminal, run `telnet localhost 9000`.
    You may then type Scheme expressions, and the server will answer
    with their evaluation. In order to exit the session, type "quit"
    on the REPL or kill Tinyscheme with `^C`; TinyScheme will then
    close the socket and exit.
    The output of some expressions will not
    be the same as you would obtain if you called TinyScheme on the
    command line directly, because the standard output is not
    redirected to the socket, but most commands will work fine.

To run the applications, just load them as Scheme files:

```
tinyscheme listhome.scm
tinyscheme smtp.scm
tinyscheme srepl.scm
```

# Documentation of TSX functions

The extension functions implemented by TinyScheme Extensions are
documented in the file `tsx-functions.txt`.

